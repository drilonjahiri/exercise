﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Contracts.Internal
{
    public class LinkStation
    {
        public Point Point { get; set; }
        public double Reach { get; set; }
        public double Power { get; set; } = 0.0;

        public bool IsValid()
        {
            if (Point == null)
            {
                return false;
            }

            return true;
        }
    }
}
