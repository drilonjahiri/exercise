﻿using Exercise.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Contracts.Internal
{
    public class ResponseBase<T> : ResponseBase
    {
        public T Content { get; set; }
    }

    public class ResponseBase
    {
        public ResponseBase()
        {
            ResultCode = ResultCode.Succeed;

        }
        public ResultCode ResultCode { get; set; }
        public string ResultDescription { get; set; }
    }
}
