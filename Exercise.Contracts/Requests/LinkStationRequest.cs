﻿using Exercise.Contracts.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Contracts.Requests
{
    public class LinkStationRequest
    {
        public List<LinkStation> LinkStations { get; set; }
        public Point DeviceLocation { get; set; }         
    }
}
