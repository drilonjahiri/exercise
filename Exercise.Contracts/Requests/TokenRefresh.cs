﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Contracts.Requests
{
    public class TokenRefresh
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
