﻿using FluentValidation;

namespace Exercise.Contracts.Requests
{
    public class UserRegistrationValidator : AbstractValidator<UserRegistration>
    {
        public UserRegistrationValidator()
        {
            // add custom validation logic
            RuleFor(x => x.Email).NotNull().NotEmpty().EmailAddress();
            RuleFor(x => x.FirstName).NotNull().NotEmpty().Length(0, 10);
        }
    }
}
