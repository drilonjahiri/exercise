﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Contracts.Requests
{
    public class LinkStationRequestValidator : AbstractValidator<LinkStationRequest>
    {
        public LinkStationRequestValidator()
        {
            // add custom validation logic
            RuleFor(x => x.LinkStations).NotNull();
            RuleFor(x => x.DeviceLocation).NotNull();
            RuleFor(x => x.LinkStations.Count).GreaterThan(0);
        }
    }
}
