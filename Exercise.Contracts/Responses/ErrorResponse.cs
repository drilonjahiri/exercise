﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Contracts.Responses
{
    public class ErrorResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class ValidationErrorResponse : ErrorResponse
    {
        public IDictionary<string, string[]> Errors { get; set; }
    }
}
