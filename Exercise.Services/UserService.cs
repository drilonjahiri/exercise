﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Exercise.Contracts.Responses;
using Exercise.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace Exercise.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<AppUser> userManager;
        private readonly IMapper mapper;
        private readonly IHttpContextAccessor context;

        public UserService(UserManager<AppUser> userManager, IMapper mapper, IHttpContextAccessor context)
        {
            this.userManager = userManager;
            this.mapper = mapper;
            this.context = context;
        }

        public async Task<AppUser> GetUserByUsername(string username)
        {
            var user = await userManager.FindByNameAsync(username);
            if (user == null)
            {
                throw new UnauthorizedAccessException("User not authorized");
            }

            return user;
        }

        public async Task<AppUser> GetCurrentUser()
        {
            var username = context.HttpContext.User.Identity.Name;
            var user = await this.GetUserByUsername(username);

            return user;    
        }
    }
}
