﻿using Exercise.Contracts.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Services
{
    public interface IDistanceService
    {
        double EuclideanDistanceBetweenTwoPoints(Point p1, Point p2);
    }
}
