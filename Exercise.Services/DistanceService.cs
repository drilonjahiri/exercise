﻿using Exercise.Contracts.Exceptions;
using Exercise.Contracts.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Services
{
    public class DistanceService : IDistanceService
    {
        public DistanceService()
        { }

        public double EuclideanDistanceBetweenTwoPoints(Point p1, Point p2)
        {
            if (p1 == null)
            {
                throw new ValidationException("Point p1 cannot be null");
            }
            if (p2 == null)
            {
                throw new ValidationException("Point p2 cannot be null");
            }

            return Math.Sqrt(((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y)));
        }
    }
}
