﻿using Exercise.Contracts.Exceptions;
using Exercise.Contracts.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Services
{
    public class LinkStationService : ILinkStationService
    {
        private readonly IDistanceService distanceService;

        public LinkStationService(IDistanceService distanceService)
        {
            this.distanceService = distanceService;
        }

        public string FindMostSuitableLinkStation(List<LinkStation> linkStations, Point deviceLocation)
        {
            var bestLinkStation = linkStations.FirstOrDefault();
            bestLinkStation.Power = 0.0;

            foreach (var linkStation in linkStations)
            {
                if (!linkStation.IsValid())
                {
                    throw new ValidationException("Link Station is not valid");
                }
                
                double distanceFromDevice = distanceService.EuclideanDistanceBetweenTwoPoints(linkStation.Point, deviceLocation);
                var power = Math.Pow(linkStation.Reach - distanceFromDevice, 2);
                if (distanceFromDevice > linkStation.Reach)
                {
                    linkStation.Power = 0.0;
                }
                else 
                {
                    linkStation.Power = power;
                }

                if (linkStation.Power > bestLinkStation.Power)
                {
                    bestLinkStation = linkStation;
                }
            }

            if (bestLinkStation.Power == 0.0)
            {
                return $"No link station within reach for point ({deviceLocation.X},{deviceLocation.Y})";
            }

            return $"Best link station for point ({deviceLocation.X},{deviceLocation.Y}) is ({bestLinkStation.Point.X},{bestLinkStation.Point.Y}) with power {bestLinkStation.Power}";
        }
    }
}
