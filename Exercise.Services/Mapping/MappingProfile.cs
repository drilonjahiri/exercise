﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exercise.Data;
using Exercise.Contracts.Requests;
using Exercise.Contracts.Responses;

namespace Exercise.Services.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AppUser, User>().ReverseMap();
            CreateMap<AppUser, UserRegistration>().ReverseMap();
        }
    }
}
