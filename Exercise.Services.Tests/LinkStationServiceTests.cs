﻿using Exercise.Contracts.Exceptions;
using Exercise.Contracts.Internal;
using Exercise.Contracts.Requests;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Exercise.Services.Tests
{
    public class LinkStationServiceTests
    {
        private IDistanceService distanceService; // NOTE: Intentionally not mocked.

        [Fact]
        public void FindMostSuitableLinkStationWhenRequestIsNotValidThrowsValidationException()
        {
            // Arrange
            var linkStationService = CreateLinkStationService();

            var request = new LinkStationRequest
            {
                LinkStations = new List<LinkStation>()
                {
                   new LinkStation
                   {
                         Point = null
                   }
                },
                DeviceLocation = new Point()
            };

            // Act and Assert
            linkStationService.Invoking(y => y.FindMostSuitableLinkStation(request.LinkStations, request.DeviceLocation))
            .Should().Throw<ValidationException>()
            .WithMessage("Link Station is not valid");
        }

        [Theory]
        [InlineData(0.0, 0.0, "Best link station for point (0,0) is (0,0) with power 100")]
        [InlineData(100.0, 100.0, "No link station within reach for point (100,100)")]
        [InlineData(15.0, 10.0, "Best link station for point (15,10) is (10,0) with power 0,6718427000252355")]
        [InlineData(18.0, 18.0, "Best link station for point (18,18) is (20,20) with power 4,715728752538098")]
        public void FindMostSuitableLinkStationReturnsExpected(double deviceLocationX, double deviceLocationY, string expectedResponse)
        {
            // Arrange
            var linkStationService = CreateLinkStationService();

            /*
              [[0, 0, 10],
              [20, 20, 5],
              [10, 0, 12]] 
            */
            var linkStations = new List<LinkStation> {
                 new LinkStation 
                 {
                    Point = new Point 
                    {
                        X = 0.0, 
                        Y = 0.0
                    },
                    Reach = 10.0
                 },
                 new LinkStation 
                 {
                    Point = new Point
                    {
                        X = 20.0,
                        Y = 20.0
                    },
                    Reach = 5.0
                 },
                 new LinkStation 
                 {
                    Point = new Point
                    {
                        X = 10.0,
                        Y = 0.0
                    },
                    Reach = 12.0
                 }
            };

            var deviceLocation = new Point
            {
                X = deviceLocationX,
                Y = deviceLocationY
            };

            // Act 
            var bestLinkStation = linkStationService.FindMostSuitableLinkStation(linkStations, deviceLocation);

            // Assert
            bestLinkStation.Should().Be(expectedResponse);
        }

        private LinkStationService CreateLinkStationService()
        {
            distanceService = new DistanceService();
            
            return new LinkStationService(distanceService);
        }
    }
}
