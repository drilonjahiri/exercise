using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Moq;
using Exercise.Contracts.Enums;
using Exercise.Contracts.Internal;
using Exercise.Contracts.Requests;
using Exercise.Contracts.Responses;
using Exercise.Data;
using Exercise.Services.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace Exercise.Services.Tests
{
    public class UserServiceTests
    {
        private Mock<UserManager<AppUser>> userManager;
        private Mock<IHttpContextAccessor> httpContextAccessor;

        [Fact]
        public async Task GetUserByUsernameReturnsExpectedResponse()
        {
            // Arrange
            var userService = CreateUserService();

            // Act
            var response = await userService.GetUserByUsername("testUser");

            // Assert
            response.FirstName.Should().Be("Test");
            response.LastName.Should().Be("User");
            response.UserName.Should().Be("testUser");
        }

        [Fact]
        public void GetUserByUsernameWhenUserNotFoundThrowsUnauuthrizedException()
        {
            // Arrange
            var userService = CreateUserService();

            userManager.Setup(x => x.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((AppUser)null);

            // Act and Assert
            userService.Invoking(y => y.GetUserByUsername("testUser"))
            .Should().Throw<UnauthorizedAccessException>()
            .WithMessage("User not authorized");
        }

        private UserService CreateUserService()
        {
            var user = new AppUser
            {
                FirstName = "Test",
                LastName = "User",
                UserName = "testUser"
            };

            if (userManager == null)
            {
                var userStoreMock = new Mock<IUserStore<AppUser>>();

                userManager = new Mock<UserManager<AppUser>>(userStoreMock.Object,
                    null, null, null, null, null, null, null, null);

                userManager.Setup(x => x.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            }

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();

            var claimPrincipalUser = new ClaimsPrincipal(
                        new ClaimsIdentity(
                            new Claim[] { new Claim("Name", "testUser") })
                        );
            httpContextAccessor = new Mock<IHttpContextAccessor>();
            httpContextAccessor.Setup(h => h.HttpContext.User).Returns(claimPrincipalUser);

            return new UserService(userManager.Object, mapper, httpContextAccessor.Object);
        }
    }
}
