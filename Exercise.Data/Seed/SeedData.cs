﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Data.Seed
{
    public class SeedData
    {
        private readonly UserManager<AppUser> userManager;

        public SeedData(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }

        public void Init()
        {
            if (!userManager.Users.Any())
            {
                var appUser = new AppUser
                {
                    Id = Guid.NewGuid(),
                    UserName = "testuser",
                    FirstName = "John",
                    LastName = "Wick",
                    Email = "testuser@admin.com"
                };

                // Note: Normally, we should not .Wait, .Result as this blocks the main thread.
                // Instead, await should be used when working async
                userManager.CreateAsync(appUser, "password").Wait();
            }
        }
    }
}
