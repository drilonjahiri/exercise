﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise.Data
{
    public class UserRole : IdentityUserRole<Guid>
    {
        public virtual AppUser User { get; set; }
        public virtual Role Role { get; set; }
    }
}
