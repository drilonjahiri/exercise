﻿using Exercise.Contracts.Exceptions;
using Exercise.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Exercise.API.Common
{
    public class ErrorResponseHelper
    {
        public ErrorResponseHelper()
        {
        }

        public ErrorResponse GetErrorResponseFromExceptionType(Exception ex)
        {
            var result = new ErrorResponse();

            switch (ex)
            {
                case UnauthorizedAccessException unex:
                    result.StatusCode = (int)HttpStatusCode.Forbidden;
                    result.Message = "Access forbidden";
                    break;
                case ValidationException vex:
                    var message = ex.Message ?? "One or more validation errors occurred";
                    result = new ValidationErrorResponse
                    {
                        Message = message,
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        Errors = vex.Failures
                    };
                    break;
                default:
                    result.StatusCode = (int)HttpStatusCode.InternalServerError;
                    result.Message = "There was an internal server error. Please try again or contact support if the issue persists.";
                    break;
            }

            return result;
        }
    }
}
