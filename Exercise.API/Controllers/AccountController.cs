﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Exercise.Data;
using Exercise.Contracts.Requests;
using Exercise.Contracts.Responses;
using Exercise.Services;
using Exercise.Contracts.Exceptions;

namespace Exercise.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ITokenService tokenService;
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly IMapper mapper;

        public AccountController(ITokenService tokenService, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager,
            IMapper mapper)
        {
            this.tokenService = tokenService;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLogin userLogin)
        {
            var user = await userManager.FindByNameAsync(userLogin.UserName);
            if (user == null)
            {
                throw new UnauthorizedAccessException("User not authorized");
            }

            var result = await signInManager.CheckPasswordSignInAsync(user, userLogin.Password, false);

            if (result.Succeeded)
            {
                var appUser = await userManager.Users.SingleOrDefaultAsync(u => u.NormalizedUserName == userLogin.UserName.ToUpper());

                var userToReturn = mapper.Map<User>(appUser);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.UserName.ToString())
                };

                var roles = await userManager.GetRolesAsync(user);
                foreach (var role in roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }

                var jwtToken = tokenService.GenerateAccessToken(claims);
                var refToken = tokenService.GenerateRefreshToken();

                user.RefreshToken = refToken;
                await userManager.UpdateAsync(user);

                return Ok(new
                {
                    accessToken = jwtToken,
                    refreshToken = refToken,
                    user = userToReturn
                });
            }

            return Unauthorized();
        }

        [AllowAnonymous]
        [HttpPost("refreshToken")]
        public async Task<IActionResult> Refresh(TokenRefresh model)
        {
            var principal = tokenService.GetPrincipalFromExpiredToken(model.Token);
            var username = principal.Identity.Name; //this is mapped to the claim "Name" by default

            var user = userManager.Users.SingleOrDefault(u => u.UserName == username);
            if (user == null || user.RefreshToken != model.RefreshToken)
            {
                throw new ValidationException("Reques model is not valid");
            }

            var newJwtToken = tokenService.GenerateAccessToken(principal.Claims);
            var newRefreshToken = tokenService.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;
            await userManager.UpdateAsync(user);

            return Ok(new
            {
                accessToken = newJwtToken,
                refreshToken = newRefreshToken
            });
        }

        [Authorize]
        [HttpPost("revoke")]
        public async Task<IActionResult> Revoke()
        {
            var username = User.Identity.Name;

            var user = userManager.Users.SingleOrDefault(u => u.UserName == username);
            if (user == null)
            {
                throw new UnauthorizedAccessException("User not authorized");
            }

            user.RefreshToken = null;

            await userManager.UpdateAsync(user);

            return NoContent();
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(UserRegistration user)
        {
            if (ModelState.IsValid)
            {
                var existingUser = await userManager.FindByNameAsync(user.Username);

                if (existingUser != null)
                {
                    throw new ValidationException("Username already exist");
                }

                var newUser = mapper.Map<AppUser>(user);

                var isCreated = await userManager.CreateAsync(newUser, user.Password);
                if (isCreated.Succeeded)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, newUser.UserName.ToString())
                    };
                    var jwtToken = tokenService.GenerateAccessToken(claims);
                    var refToken = tokenService.GenerateRefreshToken();

                    newUser.RefreshToken = refToken;
                    await userManager.UpdateAsync(newUser);

                    return Ok(new
                    {
                        accessToken = jwtToken,
                        refreshToken = refToken,
                        user = mapper.Map<User>(newUser)
                    });
                }
            }

            return BadRequest("Invalid payload");
        }

        [Authorize]
        [HttpGet("GetLoggedInUser")]
        public async Task<IActionResult> GetLoggedInUser()
        {
            var username = User.Identity.Name;

            var user = await userManager.Users.SingleOrDefaultAsync(u => u.UserName == username);
            if (user == null)
            {
                throw new UnauthorizedAccessException("User not authorized");
            }

            return Ok(mapper.Map<User>(user));
        }
    }
}
