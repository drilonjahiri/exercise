﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Exercise.Data;
using Exercise.Contracts.Requests;
using Exercise.Contracts.Responses;
using Exercise.Services;
using Exercise.API.Common;

namespace Exercise.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LinkStationController : ControllerBase
    {
        private readonly ILinkStationService linkStationService;

        public LinkStationController(ILinkStationService linkStationService)
        {
            this.linkStationService = linkStationService;
        }
        
        [HttpPost("FindMostSuitableLinkStation")]
        public IActionResult FindMostSuitableLinkStation(LinkStationRequest request)
        {
            var response = linkStationService.FindMostSuitableLinkStation(request.LinkStations, request.DeviceLocation);
            return Ok(response);
        }
    }
}
